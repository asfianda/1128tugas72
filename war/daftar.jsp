<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="id">
    <head>
        <title>Daftar</title>
    </head>
    <body>
        <h1>Daftar</h1>
        <table border="1">
            <tr>
                <th>Nama</th>
                <th>NIN</th>
                <th>Email</th>
                <th>NoHp</th>
                <th>Ket</th>
                <th>Ubah</th>
                <th>Hapus</th>
            </tr>
<c:forEach var="satu" items="${daftar}">
            <tr>
            	<td>${satu.properties.Nama}</td>
                <td>${satu.properties.NIM}</td>
                <td>${satu.properties.Email}</td>
                <td>${satu.properties.NoHp}</td>
                <td>${satu.properties.Aktif}</td>
                <td><a href="/ubah?id=${satu.key.id}">Ubah</a></td>
                <td><a href="/hapus?id=${satu.key.id}">Hapus</a></td>
            </tr>
</c:forEach>                        
        <table>
        <a href="/tambah">Tambah</a>
    </body>
</html>

